from django.shortcuts import render
from .models import TodoList


def todo_list_view(request):
    todo_list = TodoList.objects.all()
    return render(request, "todo_list.html", {"todo_list": todo_list})


# Create your views here.
